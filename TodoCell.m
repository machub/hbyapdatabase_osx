 #import "TodoCell.h"
#import "MyTodo.h"

@implementation TodoCell

@synthesize delegate = _weak_delegate;

@synthesize checkmarkButton = checkmarkButton;
@synthesize titleLabel = titleLabel;
@synthesize contextTodo= _contextTodo;

- (IBAction)didTapImageView:(id)sender
{
	id <NSObject> delegate = self.delegate;
	
    [(id <TodoCellDelegate>)delegate didTapImageViewInCell:self];
//	if ([delegate respondsToSelector:@selector(didTapImageViewInCell:)]) {
//		[(id <TodoCellDelegate>)delegate didTapImageViewInCell:self];
//	}
}

@end
