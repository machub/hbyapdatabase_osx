//
//  RootViewController.h
//  HBEditor
//
//  Created by Harsh Bhasin on 7/2/16.
//  Copyright © 2016 Harsh Bhasin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RootViewController : NSViewController
@property (nonatomic, weak) IBOutlet NSTableView *tableView;

@property (nonatomic, weak) IBOutlet NSView *ckStatusView;
@property (nonatomic, weak) IBOutlet NSTextField *ckTopStatusLabel;
@property (nonatomic, weak) IBOutlet NSTextField *ckBottomStatusLabel;


- (IBAction)suspendButtonTapped:(id)sender;
- (IBAction)resumeButtonTapped:(id)sender;
- (IBAction)deleteButtonTapped:(id)sender;
- (IBAction)addButtonTapped:(id)sender;

@end
