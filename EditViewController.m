//
//  EditViewController.m
//  HBEditor
//
//  Created by Harsh Bhasin on 7/23/16.
//  Copyright © 2016 Harsh Bhasin. All rights reserved.
//

#import "EditViewController.h"
#import "MyTodo.h"
#import "DatabaseManager.h"
#import "MyTodo.h"

@interface EditViewController ()

@end

@implementation EditViewController
@synthesize titleView = titleView;
@synthesize checkmarkButton = checkmarkButton;
@synthesize priority = priority;

@synthesize uuidLabel = uuidLabel;
@synthesize creationDateLabel = creationDateLabel;
@synthesize lastModifiedLabel = lastModifiedLabel;
@synthesize baseRecordLabel = baseRecordLabel;
@synthesize todoID = todoID;

YapDatabaseConnection *databaseConnection;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    databaseConnection = MyDatabaseManager.uiDatabaseConnection;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(databaseConnectionDidUpdate:)
                                                 name:UIDatabaseConnectionDidUpdateNotification
                                               object:nil];

    [self updateView];

}

- (void)databaseConnectionDidUpdate:(NSNotification *)notification {
    NSArray *notifications = [notification.userInfo objectForKey:kNotificationsKey];

    if ([databaseConnection hasChangeForKey:todoID inCollection:Collection_Todos inNotifications:notifications]) {
        [self updateView];
    }
}


- (void)updateView {
    __block MyTodo *todo = nil;
    // __block CKRecord *record = nil;

    if (todoID) {

        [databaseConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {

            todo = [transaction objectForKey:todoID inCollection:Collection_Todos];
            //record = [[transaction ext:Ext_CloudKit] recordForKey:todoID inCollection:Collection_Todos];
        }];
    }

    if (todo) {

        titleView.stringValue = todo.title;
        if (todo.isDone) {
            self.checkmarkButton.tag = 1;
            [self.checkmarkButton setImage:[NSImage imageNamed:@"checkmark-on"]];

        }
        else {
            self.checkmarkButton.tag = 0;
            [self.checkmarkButton setImage:[NSImage imageNamed:@"checkmark-off"]];

        }

        if (todo.priority == TodoPriorityLow)
            priority.selectedSegment = 0;
        else if (todo.priority == TodoPriorityHigh)
            priority.selectedSegment = 2;
        else
            priority.selectedSegment = 1;

        uuidLabel.stringValue = todo.uuid;

        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        df.dateStyle = NSDateFormatterMediumStyle;
        df.timeStyle = NSDateFormatterMediumStyle;

        creationDateLabel.stringValue = [df stringFromDate:todo.creationDate];
        lastModifiedLabel.stringValue = [df stringFromDate:todo.lastModified];

        //baseRecordLabel.stringValue = [record description];

    }
    else {
        titleView.stringValue = @"";
        self.checkmarkButton.tag = 0;
        priority.selectedSegment = 1;

        uuidLabel.stringValue = [[NSUUID UUID] UUIDString];
        creationDateLabel.stringValue = @"<now>";
        lastModifiedLabel.stringValue = @"<now>";

        //baseRecordLabel.stringValue = @"<New CKRecord>";
    }


}

- (IBAction)closeButtonAction:(id)sender {
    [self.presentingViewController dismissViewController:self];
}

- (IBAction)checkmarkButtonTapped:(NSButton *)sender {
    if (checkmarkButton.tag == 1) {
        checkmarkButton.tag = 0;
        [self.checkmarkButton setImage:[NSImage imageNamed:@"checkmark-off"]];
    }
    else {
        checkmarkButton.tag = 1;
        [self.checkmarkButton setImage:[NSImage imageNamed:@"checkmark-on"]];
    }
}

- (IBAction)saveButtonTapped:(NSButton *)sender {
    NSString *newTitle = titleView.stringValue;
    BOOL newIsDone = (checkmarkButton.tag == 1);

    TodoPriority newPriority;
    if (priority.selectedSegment == 0)
        newPriority = TodoPriorityLow;
    else if (priority.selectedSegment == 2)
        newPriority = TodoPriorityHigh;
    else
        newPriority = TodoPriorityNormal;
    [MyDatabaseManager.bgDatabaseConnection asyncReadWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction) {
        MyTodo *todo = [transaction objectForKey:todoID inCollection:Collection_Todos];

        if (todo == nil) {
            todo = [[MyTodo alloc] initWithUUID:uuidLabel.stringValue];
            todo.title = newTitle;
            todo.isDone = newIsDone;
            todo.priority = newPriority;
            [transaction setObject:todo forKey:todo.uuid inCollection:Collection_Todos];
        }
        else{
            todo = [todo copy]; // mutable copy

            // If the user didn't effectively make any changes,
            // we're going to try to avoid updating the properties.
            // Which, in turn, avoids uploading unchanged properties.

            if (![todo.title isEqualToString:newTitle]) {
                todo.title = newTitle;
            }
            if (todo.isDone != newIsDone) {
                todo.isDone = newIsDone;
            }
            if (todo.priority != newPriority) {
                todo.priority = newPriority;
            }

            if (todo.hasChangedProperties)
            {
                todo.lastModified = [NSDate date];
                [transaction setObject:todo forKey:todo.uuid inCollection:Collection_Todos];
            }


        }



        }];
    [self.presentingViewController dismissViewController:self];

}
@end
