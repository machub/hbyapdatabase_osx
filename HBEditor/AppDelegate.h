//
//  AppDelegate.h
//  HBEditor
//
//  Created by Harsh Bhasin on 7/1/16.
//  Copyright © 2016 Harsh Bhasin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Reachability/Reachability.h>

#import "YapDatabase.h"
#import "YapDatabaseCloudKit.h"

@class AppDelegate;

extern AppDelegate *MyAppDelegate;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (nonatomic, strong, readonly) Reachability *reachability;


@end

