//
//  AppDelegate.m
//  HBEditor
//
//  Created by Harsh Bhasin on 7/1/16.
//  Copyright © 2016 Harsh Bhasin. All rights reserved.
//

#import "AppDelegate.h"

#import "DatabaseManager.h"
#import "CloudKitManager.h"

#import "MyTodo.h"
    //#import "YapDatabaseLogging.h"

#import <CocoaLumberjack/CocoaLumberjack.h>
#import <CocoaLumberjack/DDTTYLogger.h>

#import <CloudKit/CloudKit.h>
#import <Reachability/Reachability.h>

#if DEBUG
static const NSUInteger ddLogLevel = DDLogLevelAll;
#else
static const NSUInteger ddLogLevel = DDLogLevelAll;
#endif


AppDelegate *MyAppDelegate;

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize reachability = reachability;

- (id)init
{
    if ((self = [super init]))
    {
        // Store global reference
    MyAppDelegate = self;
    [DatabaseManager initialize];
    }
    return self;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
