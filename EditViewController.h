//
//  EditViewController.h
//  HBEditor
//
//  Created by Harsh Bhasin on 7/23/16.
//  Copyright © 2016 Harsh Bhasin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MyTodo.h"

@interface EditViewController : NSViewController
@property (nonatomic, strong, readwrite) NSString *todoID;

@property (nonatomic, weak) IBOutlet NSTextField *titleView;
@property (nonatomic, weak) IBOutlet NSButton *checkmarkButton;
@property (nonatomic, weak) IBOutlet NSButton *closeButton;
@property (nonatomic, weak) IBOutlet NSSegmentedControl *priority;

@property (nonatomic, weak) IBOutlet NSTextField *uuidLabel;
@property (nonatomic, weak) IBOutlet NSTextField *creationDateLabel;
@property (nonatomic, weak) IBOutlet NSTextField *lastModifiedLabel;

@property (nonatomic, weak) IBOutlet NSTextField *baseRecordLabel;
- (IBAction)closeButtonAction:(id)sender;
- (IBAction)checkmarkButtonTapped:(NSButton *)sender;
- (IBAction)saveButtonTapped:(NSButton *)sender;

@end
