//
//  main.m
//  HBEditor
//
//  Created by Harsh Bhasin on 7/1/16.
//  Copyright © 2016 Harsh Bhasin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
