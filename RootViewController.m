    //
    //  RootViewController.m
    //  HBEditor
    //
    //  Created by Harsh Bhasin on 7/2/16.
    //  Copyright © 2016 Harsh Bhasin. All rights reserved.
    //

#import "RootViewController.h"
    //#import "EditViewController.h"
#import "TodoCell.h"
#import "DatabaseManager.h"
    //#import "CloudKitManager.h"
#import "MyTodo.h"
#import "EditViewController.h"

#import <CocoaLumberjack/CocoaLumberjack.h>

    // Log Levels: off, error, warn, info, verbose
#if DEBUG
static const NSUInteger ddLogLevel = DDLogLevelAll;
#else
static const NSUInteger ddLogLevel = DDLogLevelAll;
#endif



@interface RootViewController ()

@end

@implementation RootViewController
{
        // NSMutableArray *mapping ;
    YapDatabaseConnection *databaseConnection;
    YapDatabaseViewMappings *mappings;
    
    MyTodo *contextTodo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
        // Configure database stuff
    [self fakeDbData];
    
    databaseConnection = MyDatabaseManager.uiDatabaseConnection;
    [self initializeMappings];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(databaseConnectionDidUpdate:)
                                                 name:UIDatabaseConnectionDidUpdateNotification
                                               object:nil];
    
}


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Database Stuff
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)initializeMappings
{
    [databaseConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
        
        if ([transaction ext:Ext_View_Order])
            {
                //init all groups: you can pass in a NSArray of groups
            mappings = [[YapDatabaseViewMappings alloc] initWithGroups:@[@""] view:Ext_View_Order];
            [mappings updateWithTransaction:transaction];
            
            }
        else
            {
                // The view isn't ready yet.
                // We'll try again when we get a databaseConnectionDidUpdate notification.
            }
    }];
}

- (void)databaseConnectionDidUpdate:(NSNotification *)notification
{
    if (mappings == nil)
        {
        [self initializeMappings];
        [self.tableView reloadData];
        
        return;
        }
    NSArray *notifications = [notification.userInfo objectForKey:kNotificationsKey];
    
    NSArray *rowChanges = nil;
    
        // Since we moved our databaseConnection to a new commit,
        // we need to update the mappings too.
        // Note: the getSectionChanges:rowChanges:forNotifications:withMappings: method
        // automatically invokes the equivalent of [mappings updateWithTransaction:] for you.
    
    [[databaseConnection ext:Ext_View_Order] getSectionChanges:NULL
                                                    rowChanges:&rowChanges
                                              forNotifications:notifications
                                                  withMappings:mappings];
    
    if ([rowChanges count] == 0)
        {
            // There aren't any changes that affect our tableView
        return;
        }
    
    [self.tableView beginUpdates];
    
    
    for (YapDatabaseViewRowChange *rowChange in rowChanges)
        {
        switch (rowChange.type)
            {
                case YapDatabaseViewChangeDelete :
                {
                
                break;
                }
                case YapDatabaseViewChangeInsert :
                {
                
                break;
                }
                case YapDatabaseViewChangeMove :
                {
                break;
                }
                case YapDatabaseViewChangeUpdate :
                {
                
                break;
                }
            }
        }
    [self.tableView reloadData];
    [self.tableView endUpdates];
}
- (MyTodo *)todoAtIndexPath:(NSIndexPath *)indexPath
{
    __block MyTodo *todo = nil;
    [databaseConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
        
        todo = [[transaction ext:Ext_View_Order] objectAtIndexPath:indexPath withMappings:mappings];
    }];
    
    
    return todo;
}
    /////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions
    /////////////////////////////////////////////////////////////////////////////////////////////

- (IBAction)suspendButtonTapped:(id)sender
{
    DDLogVerbose(@"RootViewController - suspendButtonTapped:");
    
        //[MyDatabaseManager.cloudKitExtension suspend];
}

- (IBAction)resumeButtonTapped:(id)sender
{
    DDLogVerbose(@"RootViewController - resumeButtonTapped:");
    
        //[MyDatabaseManager.cloudKitExtension resume];
}
- (IBAction)deleteButtonTapped:(id)sender
{
    NSInteger selectedRow =[self.tableView selectedRow];
    if (selectedRow == -1)
        {
        return;
        }
    
    NSIndexPath *myIP = [NSIndexPath indexPathForItem:selectedRow inSection:0];
    MyTodo *todo  = [self todoAtIndexPath:myIP];
    
    
    
    YapDatabaseConnection *rwDatabaseConnection = MyDatabaseManager.bgDatabaseConnection;
    [rwDatabaseConnection asyncReadWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction) {
        
        [transaction removeObjectForKey:todo.uuid inCollection:Collection_Todos];
        
    } completionBlock:^{
        
            //[self updateStatusLabels];
    }];
    
}


- (IBAction)addButtonTapped:(id)sender{
    
    contextTodo = [[MyTodo alloc] init];
    contextTodo.title = @"New Record";
    [self performSegueWithIdentifier:@"editSegue" sender:self];

        //    int y =  (arc4random() % 501) + 500;
        //
        //    [MyDatabaseManager.bgDatabaseConnection asyncReadWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction) {
        //
        //
        //        MyTodo *first = [[MyTodo alloc] init];
        //        first.title = [NSString stringWithFormat:@"%d",y];
        //        [transaction setObject:first forKey:first.uuid inCollection:Collection_Todos];
        //
        //
        //    }];
    
}
    ////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NTableView
    /////////////////////////////////////////////////////////////////////////////////////////////



- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    
        // Get a new ViewCell
    TodoCell *cell = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
    
    NSIndexPath *myIP = [NSIndexPath indexPathForItem:row inSection:0];
    MyTodo *todo = [self todoAtIndexPath:myIP];
    
    
    cell.titleLabel.stringValue = todo.title;
    cell.contextTodo = todo;
    
    NSString *title = todo.title;
    if (title == nil)
        title = @"< missing title >";
    
    if (todo.isDone)
        {
        [cell.checkmarkButton setImage:[NSImage imageNamed:@"checkmark-on"]];
        
        }
    else
        {
        [cell.checkmarkButton setImage:[NSImage imageNamed:@"checkmark-off"]];
        
        }
    
    switch (todo.priority)
    {
        case TodoPriorityLow  : cell.titleLabel.textColor = [NSColor darkGrayColor]; break;
        case TodoPriorityHigh : cell.titleLabel.textColor = [NSColor redColor];      break;
        default               : cell.titleLabel.textColor = [NSColor blackColor];    break;
    }
    cell.delegate = self;
    
    return cell;
    
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    
    return [mappings numberOfItemsInSection:0];
    
    
    
}

    //didSelectRowAtIndexPath
-(void)tableViewSelectionDidChange:(NSNotification *)notification{
    NSInteger selectedRow = [[notification object] selectedRow];
    NSIndexPath *myIP = [NSIndexPath indexPathForItem:selectedRow inSection:0];
    contextTodo = [self todoAtIndexPath:myIP];
//    
//    NSStoryboard *storyboard = [NSStoryboard storyboardWithName:@"Main" bundle:nil];
//    EditViewController *evc = [storyboard instantiateControllerWithIdentifier:@"EditViewController"];
//    evc.contextTodo = selectedObject;
//    [self presentViewController:evc animator:nil];
    
    
    [self performSegueWithIdentifier:@"editSegue" sender:self];
}


- (void)didTapImageViewInCell:(TodoCell *)sender
{
        //switch checkmark
        //    NSInteger selectedRow =[self.tableView selectedRow];
        //    if (selectedRow == -1)
        //        {
        //        return;
        //        }
        //
        //    NSIndexPath *myIP = [NSIndexPath indexPathForItem:selectedRow inSection:0];
    MyTodo *originalTodo  = sender.contextTodo;
    
    NSString *todoID = originalTodo.uuid;
    BOOL newIsDone = originalTodo.isDone ? NO : YES;
    
    [MyDatabaseManager.bgDatabaseConnection asyncReadWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction) {
        
        MyTodo *todo = [transaction objectForKey:todoID inCollection:Collection_Todos];
        todo = [todo copy]; // make mutable copy
        
        todo.isDone = newIsDone;
        todo.lastModified = [NSDate date];
        
        [transaction setObject:todo forKey:todoID inCollection:Collection_Todos];
    }];
    
    
}
    ////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Storyboard
    /////////////////////////////////////////////////////////////////////////////////////////////
-(void) prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"editSegue"]) {
        EditViewController *vc =  [segue destinationController];
            //vc.contextTodo = contextTodo;
        vc.todoID = contextTodo.uuid;
        
    }
}


    ////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark fakes
    /////////////////////////////////////////////////////////////////////////////////////////////

- (void) fakeDbData
{
    
    
    [MyDatabaseManager.bgDatabaseConnection asyncReadWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction) {
        
        NSUInteger count = [transaction numberOfKeysInAllCollections];
        if (count > 0)
            {
            return;
            }
        
        MyTodo *first = [[MyTodo alloc] init];
        first.title = @"The First db Record";
        [transaction setObject:first forKey:first.uuid inCollection:Collection_Todos];
        
        MyTodo *second = [[MyTodo alloc] init];
        second.title = @"The Second db Record";
        [transaction setObject:second forKey:second.uuid inCollection:Collection_Todos];
        
        MyTodo *third = [[MyTodo alloc] init];
        third.title = @"The third db Record";
        [transaction setObject:third forKey:third.uuid inCollection:Collection_Todos];
        
        
    }];
    
}
    //- (NSMutableArray *) fakeData
    //{
    //    NSMutableArray *arr;
    //    MyTodo *first = [[MyTodo alloc] init];
    //    first.title = @"The First Record";
    //
    //
    //    MyTodo *second = [[MyTodo alloc] init];
    //    second.title = @"The Second Record";
    //
    //    MyTodo *third = [[MyTodo alloc] init];
    //    third.title = @"The third Record";
    //
    //    arr = [NSMutableArray arrayWithObjects:first, second, third, nil];
    //    return arr;
    //}




@end
