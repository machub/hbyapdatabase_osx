#import <Cocoa/Cocoa.h>
#import "MyTodo.h"

@interface TodoCell : NSTableCellView

@property (nonatomic, weak) id <NSObject> delegate;

@property (nonatomic, weak) IBOutlet NSButton *checkmarkButton;
@property (nonatomic, strong) IBOutlet NSTextField *titleLabel;
@property MyTodo *contextTodo;
@end

@protocol TodoCellDelegate
@optional

- (void)didTapImageViewInCell:(TodoCell *)sender;

@end
